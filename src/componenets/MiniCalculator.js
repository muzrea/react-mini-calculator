import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      result: 0
    };

    this.plusOne = this.plusOne.bind(this);
    this.minusOne = this.minusOne.bind(this);
    this.multiplyTwo = this.multiplyTwo.bind(this);
    this.divisionTwo = this.divisionTwo.bind(this);
  }

  plusOne() {
    this.setState(
      {result: this.state.result + 1}
    )
  }

  minusOne() {
    this.setState(
      {result: this.state.result - 1}
    )
  }

  multiplyTwo() {
    this.setState(
      {result: this.state.result * 2}
    )
  }

  divisionTwo() {
    this.setState(
      {result: this.state.result / 2}
    )
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <button onClick={this.plusOne}>加1</button>
          <button onClick={this.minusOne}>减1</button>
          <button onClick={this.multiplyTwo}>乘以2</button>
          <button onClick={this.divisionTwo}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

